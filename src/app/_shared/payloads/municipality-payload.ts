export interface MunicipalityPayload {
  name: string;
  countryId: number;
}
