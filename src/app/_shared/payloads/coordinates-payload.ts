export interface CoordinatesPayload {
  lat: number;
  lng: number;
}
