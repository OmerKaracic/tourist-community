import {CoordinatesPayload} from './coordinates-payload';

export interface LandmarkPayload {
  name: string;
  description: string;
  coordinates: CoordinatesPayload;
  importanceId: number;
  municipalityId: number;
  active: boolean;
}
