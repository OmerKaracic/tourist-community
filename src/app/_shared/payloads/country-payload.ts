export interface CountryPayload {
  name: string;
  code: string;
}
