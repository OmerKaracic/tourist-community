import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {ApiConfiguration} from './api-configuration';
import {CountryControllerService} from './services/country-controller.service';
import {MunicipalityControllerService} from './services/municipality-controller.service';
import {LandmarkControllerService} from './services/landmark-controller.service';
import {ImportanceControllerService} from './services/importance-controller.service';

/**
 * Module that provides instances for all API services
 */
@NgModule({
  imports: [
    HttpClientModule
  ],
  exports: [
    HttpClientModule
  ],
  declarations: [],
  providers: [
    ApiConfiguration,
    CountryControllerService,
    MunicipalityControllerService,
    LandmarkControllerService,
    ImportanceControllerService
  ]
})
export class ApiModule { }
