/* tslint:disable */

import {Injectable} from '@angular/core';
import {BaseService} from '../base-service';
import {ApiConfiguration} from '../api-configuration';
import {HttpClient, HttpHeaders, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {Municipality} from '../models/municipality';
import {MunicipalityPayload} from '../payloads/municipality-payload';

@Injectable()
class MunicipalityControllerService extends BaseService {

  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param params The `MunicipalityControllerService.GetMunicipalityByIdUsingGETParams` containing the following parameters:
   *
   * - `municipalityId`: number
   *
   * @return Observable<Municipality | null>
   */
  getMunicipalityByIdUsingGETResponse(params: MunicipalityControllerService.GetMunicipalityByIdUsingGETParams): Observable<HttpResponse<Municipality>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/municipality/${params.municipalityId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: Municipality;
        _body = _resp.body as Municipality;
        return _resp.clone({body: _body}) as HttpResponse<Municipality>;
      })
    );
  }

  getMunicipalityIdUsingGET(params: MunicipalityControllerService.GetMunicipalityByIdUsingGETParams): Observable<Municipality | null> {
    return this.getMunicipalityByIdUsingGETResponse(params).pipe(
      map(_r => _r.body)
    );
  }

  /**
   * @param params The `MunicipalityControllerService.GetMunicipalityMunicipalityIdUsingGETParams` containing the following parameters:
   *
   * - `countryId`: number
   *
   * @return Observable<Array<Municipality> | null>
   */
  getMunicipalitiesByCountryUsingGETResponse(params: MunicipalityControllerService.GetMunicipalitiesByMunicipalityUsingGETParams): Observable<HttpResponse<Array<Municipality>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/municipality/country/${params.countryId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: Array<Municipality>;
        _body = _resp.body as Array<Municipality>;
        return _resp.clone({body: _body}) as HttpResponse<Array<Municipality>>;
      })
    );
  }

  getMunicipalitiesByCountryUsingGET(params: MunicipalityControllerService.GetMunicipalitiesByMunicipalityUsingGETParams): Observable<Array<Municipality> | null> {
    return this.getMunicipalitiesByCountryUsingGETResponse(params).pipe(
      map(_r => _r.body)
    );
  }

  /**
   * @param params The `MunicipalityControllerService.CreateMunicipalityUsingPOSTParams` containing the following parameters:
   *
   * - `country`: Municipality
   *
   * @return Observable<Municipality>
   */
  createMunicipalityUsingPOSTResponse(params: MunicipalityControllerService.CreateMunicipalityUsingPOSTParams): Observable<HttpResponse<Municipality>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any;
    __body = params.municipalityPayload;

    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/municipality`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: Municipality;
        _body = _resp.body as Municipality;
        return _resp.clone({body: _body}) as HttpResponse<Municipality>;
      })
    );
  }

  createMunicipalityUsingPOST(params: MunicipalityControllerService.CreateMunicipalityUsingPOSTParams): Observable<Municipality | null> {
    return this.createMunicipalityUsingPOSTResponse(params).pipe(
      map(_r => _r.body)
    );
  }
}

module MunicipalityControllerService {

  /**
   * Parameters for getMunicipalityByIdUsingGET
   */
  export interface GetMunicipalityByIdUsingGETParams {
    municipalityId: number;
  }

  /**
   * Parameters for getMunicipalityByIdUsingGET
   */
  export interface GetMunicipalitiesByMunicipalityUsingGETParams {
    countryId: number;
  }

  /**
   * Parameters for createMunicipalityUsingPOST
   */
  export interface CreateMunicipalityUsingPOSTParams {
    municipalityPayload: MunicipalityPayload
  }
}

export { MunicipalityControllerService };
