/* tslint:disable */

import {Injectable} from '@angular/core';
import {BaseService} from '../base-service';
import {ApiConfiguration} from '../api-configuration';
import {HttpClient, HttpHeaders, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {Importance} from '../models/importance';

@Injectable()
class ImportanceControllerService extends BaseService {

  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param params The `ImportanceControllerService.GetImportanceByIdUsingGETParams` containing the following parameters:
   *
   * - `importanceId`: number
   *
   * @return Observable<Importance | null>
   */
  getImportanceByIdUsingGETResponse(params: ImportanceControllerService.GetImportanceByIdUsingGETParams): Observable<HttpResponse<Importance>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/importance/${params.importanceId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: Importance;
        _body = _resp.body as Importance;
        return _resp.clone({body: _body}) as HttpResponse<Importance>;
      })
    );
  }

  getImportanceByIdUsingGET(params: ImportanceControllerService.GetImportanceByIdUsingGETParams): Observable<Importance | null> {
    return this.getImportanceByIdUsingGETResponse(params).pipe(
      map(_r => _r.body)
    );
  }

  /**
   * @return Observable<Array<Importance> | null>
   */
  getImportancesUsingGETResponse(): Observable<HttpResponse<Array<Importance>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/importance/type`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: Array<Importance>;
        _body = _resp.body as Array<Importance>;
        return _resp.clone({body: _body}) as HttpResponse<Array<Importance>>;
      })
    );
  }

  getImportancesByUsingGET(): Observable<Array<Importance> | null> {
    return this.getImportancesUsingGETResponse().pipe(
      map(_r => _r.body)
    );
  }
}

module ImportanceControllerService {

  /**
   * Parameters for getImportanceByIdUsingGET
   */
  export interface GetImportanceByIdUsingGETParams {
    importanceId: number;
  }
}

export { ImportanceControllerService };
