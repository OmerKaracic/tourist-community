/* tslint:disable */

import {Injectable} from '@angular/core';
import {BaseService} from '../base-service';
import {ApiConfiguration} from '../api-configuration';
import {HttpClient, HttpHeaders, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {Landmark} from '../models/landmark';
import {LandmarkPayload} from '../payloads/landmark-payload';

@Injectable()
class LandmarkControllerService extends BaseService {

  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @return Observable<Array<Landmark> | null>
   */
  getLandmarksUsingGETResponse(): Observable<HttpResponse<Array<Landmark>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/landmark`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: Array<Landmark>;
        _body = _resp.body as Array<Landmark>;
        return _resp.clone({body: _body}) as HttpResponse<Array<Landmark>>;
      })
    );
  }

  getLandmarksUsingGET(): Observable<Array<Landmark> | null> {
    return this.getLandmarksUsingGETResponse().pipe(
      map(_r => _r.body)
    );
  }

  /**
   * @param params The `LandmarkControllerService.GetLandmarkByIdUsingGETParams` containing the following parameters:
   *
   * - `landmarkId`: number
   *
   * @return Observable<Landmark | null>
   */
  getLandmarkByIdUsingGETResponse(params: LandmarkControllerService.GetLandmarkByIdUsingGETParams): Observable<HttpResponse<Landmark>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/landmark/${params.landmarkId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: Landmark;
        _body = _resp.body as Landmark;
        return _resp.clone({body: _body}) as HttpResponse<Landmark>;
      })
    );
  }

  getLandmarkByIdUsingGET(params: LandmarkControllerService.GetLandmarkByIdUsingGETParams): Observable<Landmark | null> {
    return this.getLandmarkByIdUsingGETResponse(params).pipe(
      map(_r => _r.body)
    );
  }

  /**
   * @param params The `LandmarkControllerService.GetLandmarkMunicipalityIdUsingGETParams` containing the following parameters:
   *
   * - `municipalityId`: number
   *
   * @return Observable<Array<Landmark> | null>
   */
  getLandmarksByMunicipalityUsingGETResponse(params: LandmarkControllerService.GetLandmarksByMunicipalityUsingGETParams): Observable<HttpResponse<Array<Landmark>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/landmark/municipality/${params.municipalityId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: Array<Landmark>;
        _body = _resp.body as Array<Landmark>;
        return _resp.clone({body: _body}) as HttpResponse<Array<Landmark>>;
      })
    );
  }

  getLandmarksByMunicipalityUsingGET(params: LandmarkControllerService.GetLandmarksByMunicipalityUsingGETParams): Observable<Array<Landmark> | null> {
    return this.getLandmarksByMunicipalityUsingGETResponse(params).pipe(
      map(_r => _r.body)
    );
  }

  /**
   * @param params The `LandmarkControllerService.SearchLandmarksUsingGETParams` containing the following parameters:
   *
   * - `name`: string
   * - `importance`: string
   *
   * @return Observable<Array<Landmark> | null>
   */
  searchLandmarksUsingGETResponse(params: LandmarkControllerService.SearchLandmarksUsingGETParams): Observable<HttpResponse<Array<Landmark>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/landmark/search?n=${params.name}&i=${params.importance}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: Array<Landmark>;
        _body = _resp.body as Array<Landmark>;
        return _resp.clone({body: _body}) as HttpResponse<Array<Landmark>>;
      })
    );
  }

  searchLandmarksUsingGET(params: LandmarkControllerService.SearchLandmarksUsingGETParams): Observable<Array<Landmark> | null> {
    return this.searchLandmarksUsingGETResponse(params).pipe(
      map(_r => _r.body)
    );
  }

  /**
   * @param params The `LandmarkControllerService.CreateLandmarkUsingPOSTParams` containing the following parameters:
   *
   * - `country`: Landmark
   *
   * @return Observable<Landmark | null>
   */
  createLandmarkUsingPOSTResponse(params: LandmarkControllerService.CreateLandmarkUsingPOSTParams): Observable<HttpResponse<Landmark>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any;
    __body = params.landmarkPayload;

    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/landmark`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: Landmark;
        _body = _resp.body as Landmark;
        return _resp.clone({body: _body}) as HttpResponse<Landmark>;
      })
    );
  }

  createLandmarkUsingPOST(params: LandmarkControllerService.CreateLandmarkUsingPOSTParams): Observable<Landmark | null> {
    return this.createLandmarkUsingPOSTResponse(params).pipe(
      map(_r => _r.body)
    );
  }

  /**
   * @param params The `LandmarkControllerService.UpdateLandmarkUsingPUTParams` containing the following parameters:
   *
   * - `landmarkId`: number
   * - `landmarkPayload`: LandmarkPayload
   *
   * @return Observable<Landmark | null>
   */
  updateLandmarkUsingPUTResponse(params: LandmarkControllerService.UpdateLandmarkUsingPUTParams): Observable<HttpResponse<Landmark>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any;
    __body = params.landmarkPayload;

    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/landmark/${params.landmarkId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: Landmark;
        _body = _resp.body as Landmark;
        return _resp.clone({body: _body}) as HttpResponse<Landmark>;
      })
    );
  }

  updateLandmarkUsingPUT(params: LandmarkControllerService.UpdateLandmarkUsingPUTParams): Observable<Landmark | null> {
    return this.updateLandmarkUsingPUTResponse(params).pipe(
      map(_r => _r.body)
    );
  }

  /**
   * @param params The `LandmarkControllerService.ToggleActiveUsingPUTParams` containing the following parameters:
   *
   * - `landmarkId`: number
   * - `isActive`: boolean
   *
   * @return Observable<Landmark | null>
   */
  toggleActiveUsingPUTResponse(params: LandmarkControllerService.ToggleActiveUsingPUTParams): Observable<HttpResponse<Landmark>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any;

    let req = new HttpRequest<any>(
      'PUT',
      this.rootUrl + `/api/landmark/${params.landmarkId}/${params.isActive}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<Landmark>;
        let _body: Landmark;
        _body = _resp.body as Landmark;
        return _resp.clone({body: _body}) as HttpResponse<Landmark>;
      })
    );
  }

  toggleActiveUsingPUT(params: LandmarkControllerService.ToggleActiveUsingPUTParams): Observable<Landmark | null> {
    return this.toggleActiveUsingPUTResponse(params).pipe(
      map(_r => _r.body)
    );
  }

  removeLandmarkUsingDELETEResponse(params: LandmarkControllerService.RemoveLandmarkUsingDELETEParams): Observable<HttpResponse<void>> {
  let __params = this.newParams();
  let __headers = new HttpHeaders();
  let __body: any = null;

  let req = new HttpRequest<any>(
    'DELETE',
    this.rootUrl + `/api/landmark/${params.landmarkId}`,
    __body,
    {
      headers: __headers,
      params: __params,
      responseType: 'json'
    });

  return this.http.request<any>(req).pipe(
    filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<void>;
        let _body: null = null;

        return _resp.clone({body: _body}) as HttpResponse<void>;
      })
    );
  }

  removeLandmarkUsingDELETE(params: LandmarkControllerService.RemoveLandmarkUsingDELETEParams): Observable<any> {
    return this.removeLandmarkUsingDELETEResponse(params).pipe(
      map(_r => _r.body)
    );
  }
}

module LandmarkControllerService {

  /**
   * Parameters for getLandmarkByIdUsingGET
   */
  export interface GetLandmarkByIdUsingGETParams {
    landmarkId: number;
  }

  /**
   * Parameters for getLandmarkByIdUsingGET
   */
  export interface GetLandmarksByMunicipalityUsingGETParams {
    municipalityId: number;
  }

  /**
   * Parameters for searchLandmarksUsingGET
   */
  export interface SearchLandmarksUsingGETParams {
    name?: string;
    importance?: string;
  }

  /**
   * Parameters for createLandmarkUsingPOST
   */
  export interface CreateLandmarkUsingPOSTParams {
    landmarkPayload: LandmarkPayload;
  }

  /**
   * Params for updateLandmarkUsingPUT
   */
  export interface UpdateLandmarkUsingPUTParams {
    landmarkId: number;
    landmarkPayload: LandmarkPayload
  }

  /**
   * Params for toggleActiveUsingPUT
   */
  export interface ToggleActiveUsingPUTParams {
    landmarkId: number;
    isActive: boolean;
  }

  /**
   * Params for removeLandmarkUsingDELETE
   */
  export interface RemoveLandmarkUsingDELETEParams {
    landmarkId: number;
  }
}

export { LandmarkControllerService };
