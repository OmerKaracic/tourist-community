/* tslint:disable */

import {Injectable} from '@angular/core';
import {BaseService} from '../base-service';
import {ApiConfiguration} from '../api-configuration';
import {HttpClient, HttpHeaders, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {Country} from '../models/country';
import {CountryPayload} from '../payloads/country-payload';

@Injectable()
class CountryControllerService extends BaseService {

  constructor(
    config: ApiConfiguration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @return Observable<Array<Country> | null>
   */
  getCountriesUsingGETResponse(): Observable<HttpResponse<Array<Country>>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/country`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: Array<Country>;
        _body = _resp.body as Array<Country>;
        return _resp.clone({body: _body}) as HttpResponse<Array<Country>>;
      })
    );
  }

  getCountriesUsingGET(): Observable<Array<Country> | null> {
    return this.getCountriesUsingGETResponse().pipe(
      map(_r => _r.body)
    );
  }

  /**
   * @param params The `CountryControllerService.GetCountryByIdUsingGETParams` containing the following parameters:
   *
   * - `countryId`: number
   *
   * @return Observable<Country | null>
   */
  getCountryByIdUsingGETResponse(params: CountryControllerService.GetCountryByIdUsingGETParams): Observable<HttpResponse<Country>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/country/${params.countryId}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: Country;
        _body = _resp.body as Country;
        return _resp.clone({body: _body}) as HttpResponse<Country>;
      })
    );
  }

  getCountryByIdUsingGET(params: CountryControllerService.GetCountryByIdUsingGETParams): Observable<Country | null> {
    return this.getCountryByIdUsingGETResponse(params).pipe(
      map(_r => _r.body)
    );
  }

  /**
   * @param params The `CountryControllerService.CreateCountryUsingPOSTParams` containing the following parameters:
   *
   * - `country`: Country
   *
   * @return Observable<Country>
   */
  createCountryUsingPOSTResponse(params: CountryControllerService.CreateCountryUsingPOSTParams): Observable<HttpResponse<Country>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any;
    __body = params.countryPayload;

    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/country`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      filter(_r => _r instanceof HttpResponse),
      map(_r => {
        let _resp = _r as HttpResponse<any>;
        let _body: Country;
        _body = _resp.body as Country;
        return _resp.clone({body: _body}) as HttpResponse<Country>;
      })
    );
  }

  createCountryUsingPOST(params: CountryControllerService.CreateCountryUsingPOSTParams): Observable<Country | null> {
    return this.createCountryUsingPOSTResponse(params).pipe(
      map(_r => _r.body)
    );
  }
}

module CountryControllerService {

  /**
   * Parameters for getCountryByIdUsingGET
   */
  export interface GetCountryByIdUsingGETParams {
    countryId: number;
  }

  /**
   * Parameters for createCountryUsingPOST
   */
  export interface CreateCountryUsingPOSTParams {
    countryPayload: CountryPayload
  }
}

export { CountryControllerService };
