import {Municipality} from './municipality';
import {Photo} from './photo';
import {Coordinates} from './coordinates';
import {Importance} from './importance';
import {Review} from './review';

export interface Landmark {
  id: number;
  name: string;
  description: string;
  photos: Array<Photo>;
  coordinates: Coordinates;
  importance: Importance;
  reviews: Array<Review>;
  municipality: Municipality;
  active: boolean;
}
