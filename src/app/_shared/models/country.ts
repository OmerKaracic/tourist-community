import {Municipality} from './municipality';

export interface Country {
  id: number;
  name: string;
  code: string;
  municipalities?: Array<Municipality>;
}
