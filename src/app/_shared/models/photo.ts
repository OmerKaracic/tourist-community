import {Landmark} from './landmark';

export interface Photo {
  id: number;
  path: string;
  landmark?: Landmark;
}
