import {Landmark} from './landmark';
import {ReviewType} from './review-type';

export interface Review {
  id: number;
  landmark?: Landmark;
  reviewType: ReviewType;
}
