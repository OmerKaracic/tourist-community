export interface Coordinates {
  id: number;
  lat: number;
  lng: number;
}
