import {Country} from './country';
import {Landmark} from './landmark';

export interface Municipality {
  id: number;
  name: string;
  country: Country;
  landmarks?: Array<Landmark>;
}
