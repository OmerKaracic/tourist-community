export interface Importance {
  id: number;
  name: string;
}
