import { Component, OnInit } from '@angular/core';
import {Landmark} from '../../../_shared/models/landmark';
import {MdbModalRef} from 'mdb-angular-ui-kit/modal';
import {LandmarkControllerService} from '../../../_shared/services/landmark-controller.service';
import {LandmarkPayload} from '../../../_shared/payloads/landmark-payload';
import {Observable} from 'rxjs';
import {NgForm} from '@angular/forms';
import {Importance} from '../../../_shared/models/importance';
import {ImportanceControllerService} from '../../../_shared/services/importance-controller.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.scss']
})
export class EditModalComponent implements OnInit {

  landmarkData: Landmark | undefined;
  importanceTypes: Array<Importance> = [];

  constructor(
    public modalRef: MdbModalRef<EditModalComponent>,
    private landmarkService: LandmarkControllerService,
    private importanceService: ImportanceControllerService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    console.log('* Editing landmark...', this.landmarkData);
    this.importanceService.getImportancesByUsingGET().subscribe((importanceTypes) => {
      if (importanceTypes !== null) {
        this.importanceTypes = importanceTypes;
      }
    });
  }

  onLandmarkUpdate(form: NgForm): void {
    if (this.landmarkData) {
      let importanceId = 0;
      this.importanceTypes.forEach(i => {
        if (i.name === form.value.importance) {
          importanceId = i.id;
        }
      });
      const landmarkPayload: LandmarkPayload = {
        name: form.value.name,
        description: form.value.description,
        coordinates: this.landmarkData.coordinates,
        municipalityId: this.landmarkData.municipality.id,
        active: this.landmarkData.active,
        importanceId
      };
      this.updateLandmark(this.landmarkData.id, landmarkPayload).subscribe((updatedLandmark) => {
        if (updatedLandmark !== null) {
          this.modalRef.close(updatedLandmark);
        } else {
          this.toastr.error('There was an error while updating landmark.');
        }
      });
    }
  }

  updateLandmark(landmarkId: number, landmarkPayload: LandmarkPayload): Observable<Landmark | null> {
    console.log('* Updating landmark ID :: ' + landmarkId);
    return this.landmarkService.updateLandmarkUsingPUT({landmarkId, landmarkPayload});
  }
}
