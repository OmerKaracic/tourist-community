import {Component, Input, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {LandmarkPayload} from '../../_shared/payloads/landmark-payload';
import {CoordinatesPayload} from '../../_shared/payloads/coordinates-payload';
import {Observable} from 'rxjs';
import {Landmark} from '../../_shared/models/landmark';
import {LandmarkControllerService} from '../../_shared/services/landmark-controller.service';
import {Municipality} from '../../_shared/models/municipality';
import {Country} from '../../_shared/models/country';
import {ImportanceControllerService} from '../../_shared/services/importance-controller.service';
import {Importance} from '../../_shared/models/importance';
import {MdbModalRef, MdbModalService} from 'mdb-angular-ui-kit/modal';
import {EditModalComponent} from './edit-modal/edit-modal.component';
import {ToastrService} from 'ngx-toastr';
import {MdbCheckboxChange} from 'mdb-angular-ui-kit/checkbox';

@Component({
  selector: 'app-landmarks',
  templateUrl: './landmarks.component.html',
  styleUrls: ['./landmarks.component.scss']
})
export class LandmarksComponent implements OnInit {

  @Input() countries: Array<Country> = [];
  municipalities: Array<Municipality> = [];
  landmarks: Array<Landmark> = [];
  importanceTypes: Array<Importance> = [];
  selectedCountryId: any;
  selectedMunicipalityId: any;
  selectedImportanceId: any;
  modalRef: MdbModalRef<EditModalComponent> | undefined;

  constructor(
    private landmarkService: LandmarkControllerService,
    private importanceService: ImportanceControllerService,
    private modalService: MdbModalService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.importanceService.getImportancesByUsingGET().subscribe((importanceTypes) => {
      if (importanceTypes !== null) {
        this.importanceTypes = importanceTypes;
      }
    });
    this.landmarkService.getLandmarksUsingGET().subscribe((landmarks) => {
      if (landmarks !== null) {
        this.landmarks = landmarks;
      }
    });
  }

  onLandmarkAdd(form: NgForm): void {
    const coordinatesPayload: CoordinatesPayload = {
      lat: form.value.lat,
      lng: form.value.lng
    };
    const landmarkPayload: LandmarkPayload = {
      name: form.value.name,
      description: form.value.description,
      coordinates: coordinatesPayload,
      municipalityId: +this.selectedMunicipalityId,
      importanceId: +this.selectedImportanceId,
      active: true
    };

    form.resetForm();

    this.createLandmark(landmarkPayload).subscribe((landmark) => {
      if (landmark !== null) {
        console.log('* Landmark successfully created.');
        this.landmarks.push(landmark);
        this.toastr.success('Landmark successfully created');
      } else {
        this.toastr.error('There was an error while creating landmark');
      }
    });
  }

  createLandmark(landmarkPayload: LandmarkPayload): Observable<Landmark | null> {
    console.log('* Creating new landmark', landmarkPayload);
    return this.landmarkService.createLandmarkUsingPOST({landmarkPayload});
  }

  getMunicipalitiesFromSelectedCountry(): void {
    this.countries.forEach(country => {
      if (country.id === +this.selectedCountryId) {
        console.log('selected country municipalities', country);
        this.municipalities = country.municipalities ? country.municipalities : [];
        console.log('municipalities', this.municipalities);
      }
    });
  }

  editLandmark(landmarkData: Landmark): void {
    this.modalRef = this.modalService.open(EditModalComponent, {data: {landmarkData}});
    this.modalRef.onClose.subscribe((updatedLandmarkObject: Landmark) => {
      if (updatedLandmarkObject) {
        this.landmarks = this.landmarks.map(l => {
          return l.id === updatedLandmarkObject.id ? updatedLandmarkObject : l;
        });
        this.toastr.success('Landmark successfully updated.');
      }
    });
  }

  deleteLandmark(landmarkId: number): void {
    this.landmarkService.removeLandmarkUsingDELETE({landmarkId}).subscribe((response) => {
      console.log('response', response);
      this.landmarks = this.landmarks.filter(l => l.id !== landmarkId);
      console.log('* Landmark deleted successfully...', landmarkId);
      this.toastr.success('Landmark deleted successfully.');
    });
  }

  toggleActive(landmarkId: number, $event: MdbCheckboxChange): void {
    const isActive = $event.checked;
    this.landmarkService.toggleActiveUsingPUT({landmarkId, isActive}).subscribe((landmark) => {
      console.log(landmark);
      if (landmark !== null) {
        switch (landmark.active) {
          case true:
            this.toastr.success('Landmark activated successfully'); break;
          case false:
            this.toastr.success('Landmark deactivated successfully'); break;
        }
      }
    });
  }
}
