import {Component, OnInit, Output} from '@angular/core';
import {Country} from '../_shared/models/country';
import {CountryControllerService} from '../_shared/services/country-controller.service';
import {Observable} from 'rxjs';
import {Municipality} from '../_shared/models/municipality';
import {MunicipalityControllerService} from '../_shared/services/municipality-controller.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  @Output() countries: Array<Country> = [];

  constructor(
    private countryService: CountryControllerService,
    private municipalityService: MunicipalityControllerService
  ) { }

  ngOnInit(): void {
    this.getCountries().subscribe((countries) => {
      if (countries !== null) {
        this.countries = countries;
        console.log('countries', this.countries);
        this.countries.forEach(country => {
          this.getMunicipalitiesByCountry(country.id).subscribe((municipalities) => {
            if (municipalities !== null) {
              country.municipalities = municipalities;
            }
          });
        });
      }
    });
  }

  getCountries(): Observable<Array<Country> | null> {
    return this.countryService.getCountriesUsingGET();
  }

  getMunicipalitiesByCountry(countryId: number): Observable<Array<Municipality> | null> {
    return this.municipalityService.getMunicipalitiesByCountryUsingGET({countryId});
  }
}
