import {Component, Input, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Country} from '../../_shared/models/country';
import {CountryControllerService} from '../../_shared/services/country-controller.service';
import {Observable} from 'rxjs';
import {CountryPayload} from '../../_shared/payloads/country-payload';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.scss']
})
export class CountriesComponent implements OnInit {

  @Input() countries: Array<Country> = [];

  constructor(
    private countryService: CountryControllerService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
  }

  onCountryAdd(form: NgForm): void {
    const countryPayload: CountryPayload = {
      name: form.value.name,
      code: form.value.code
    };

    form.resetForm();

    this.createCountry(countryPayload).subscribe((country) => {
      if (country !== null) {
        this.countries.push(country);
        this.toastr.success('Country successfully created');
      } else {
        this.toastr.error('There was an error while creating country');
      }
    });
  }

  editCountry(id: number): void {
    console.log('Editing country by ID :: ' + id);
    this.toastr.info('Work in progress...');
  }

  deleteCountry(id: number): void {
    console.log('Deleting country by ID :: ' + id);
    this.toastr.info('Work in progress...');
  }

  createCountry(countryPayload: CountryPayload): Observable<Country | null> {
    return this.countryService.createCountryUsingPOST({countryPayload});
  }
}
