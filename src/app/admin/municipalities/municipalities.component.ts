import {Component, Input, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Country} from '../../_shared/models/country';
import {Municipality} from '../../_shared/models/municipality';
import {Observable} from 'rxjs';
import {MunicipalityControllerService} from '../../_shared/services/municipality-controller.service';
import {MunicipalityPayload} from '../../_shared/payloads/municipality-payload';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-municipalities',
  templateUrl: './municipalities.component.html',
  styleUrls: ['./municipalities.component.scss']
})
export class MunicipalitiesComponent implements OnInit {

  @Input() countries: Array<Country> = [];
  municipalities: Array<Municipality> = [];
  selectedCountryId: any;
  selectedCountry: Country | undefined;

  constructor(
    private municipalityService: MunicipalityControllerService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.countries.forEach(country => {
      if (country.municipalities) {
        country.municipalities.forEach(municipality => {
          this.municipalities.push(municipality);
        });
      }
    });
    console.log('municipalities', this.municipalities);
  }

  selectCountry(): void {
    console.log('this.selectedCountryId: ' + this.selectedCountryId);
    this.countries.forEach(country => {
      if (country.id === +this.selectedCountryId) {
        this.selectedCountry = country;
        console.log('this.selectedCountry: ' + this.selectedCountry);
      }
    });
  }

  onMunicipalityAdd(form: NgForm): void {
    if (this.selectedCountry) {
      const municipalityPayload: MunicipalityPayload = {
        name: form.value.name,
        countryId: this.selectedCountry.id
      };

      form.resetForm();

      this.createMunicipality(municipalityPayload).subscribe((municipality) => {
        if (municipality !== null && this.selectedCountry) {
          municipality.country = this.selectedCountry;
          this.municipalities.push(municipality);
          this.toastr.success('Municipality successfully created');
        } else {
          console.error('* There was an error while creating municipality :: ' + this.selectedCountry);
          this.toastr.error('There was an error while creating municipality');
        }
      });
    }
  }

  editMunicipality(id: number): void {
    console.log('Editing municipality by ID :: ' + id);
    this.toastr.info('Work in progress...');
  }

  deleteMunicipality(id: number): void {
    console.log('Deleting municipality by ID :: ' + id);
    this.toastr.info('Work in progress...');
  }

  createMunicipality(municipalityPayload: MunicipalityPayload): Observable<Municipality | null> {
    return this.municipalityService.createMunicipalityUsingPOST({municipalityPayload});
  }
}
