import {Component, OnInit} from '@angular/core';
import {LandmarkControllerService} from './_shared/services/landmark-controller.service';
import {Landmark} from './_shared/models/landmark';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  landmarks: Array<Landmark> = [];

  constructor(
    private landmarkService: LandmarkControllerService
  ) {}

  ngOnInit(): void {
    const municipalityId = 1;
    this.landmarkService.getLandmarksByMunicipalityUsingGET({municipalityId}).subscribe((landmarks) => {
      console.log(landmarks);
      if (landmarks !== null) {
        this.landmarks = landmarks;
      }
    });
  }
}
